/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareagrupo4;

/**
 *
 * @author Driem
 */
public class Deportista extends Persona{
    
    private String Deporte;
    private String Marca;
    
    public Deportista(String Nombre, double Altura, double Peso, String Apellido,String Deporte,String Marca) {
        
        super(Nombre, Altura, Peso, Apellido);
        this.Deporte = Deporte;
        this.Marca = Marca;
    }
    
       public void SetDeporte(String Deporte)
    {
        this.Deporte = Deporte;
    }
    
    public void SetMarca(String Marca)
    {
        this.Marca = Marca;
    }
        public String GetDeporte()
    {
        return this.Deporte;
    }
    
     public String GetMarca()
    {
        return this.Marca;
    }
    
    
     public String getDetalle(){
         
        return "\n Es Deportista y practica :" + this.Deporte + " y su marca es:" + this.Marca + " ";
         
     }
}
