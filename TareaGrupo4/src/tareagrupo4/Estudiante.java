/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareagrupo4;

/**
 *
 * @author frank
 */
public class Estudiante extends Persona{
    //Declaramos los atributos 
    private String Carrera;
    private String CentroDeEstudio;
    //Declaramos constructor con herencia
    public Estudiante(String Nombre, double Altura, double Peso, String Apellido,String Carrera,String CentroDeEstudio) {
        super(Nombre, Altura, Peso, Apellido);
        this.Carrera = Carrera;
        this.CentroDeEstudio = CentroDeEstudio;
    }
    
       public void SetCarrera(String Carrera)
    {
        this.Carrera = Carrera;
    }
    
    public void SetCentroDeEstudio(String CentroDeEstudio)
    {
        this.CentroDeEstudio = CentroDeEstudio;
    }
        public String GetCarrera()
    {
        return this.Carrera;
    }
    
     public String GetCentroDeEstudio()
    {
        return this.CentroDeEstudio;
    }
    
    
     public String getDetalle(){
         
        return "\n Estudia la carrera de:" + this.Carrera + "Estudia en:" + this.CentroDeEstudio ;
         
     }
  
    
}
