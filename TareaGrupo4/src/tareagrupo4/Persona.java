package tareagrupo4;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Driem
 */
public class Persona {
       //Declaramos atributos
    private String Nombre;
    private String Apellido;
    private double Altura;
    private double Peso;


    //Declaramos el constructor
    public Persona(String Nombre,double Altura, double Peso, String Apellido)
    {
        this.Nombre = Nombre;
        this.Altura = Altura;
        this.Peso = Peso;
        this.Apellido = Apellido;
           
    }

    //Declaramos funciones de ancapsulamiento de atributos
    public void SetNombre(String Nombre)
    {
        this.Nombre = Nombre;
    }
    
    public void SetApellido(String Apellido)
    {
        this.Apellido = Apellido;
    }
    public void setAltura(double Altura)
    {
        this.Altura = Altura;
    }
    
    public void setPeso(int Peso)
    {
        this.Peso = Peso;
    }

    public double GetAltura()
    {
        return this.Altura;
    }
    
    public double GetPeso()
    {
        return this.Peso;
    }
    
    public String GetNombre()
    {
        return this.Nombre;
    }
    
     public String GetApellido()
    {
        return this.Apellido;
    }
    
    
//Declaramos la clase getAtributos como clase para ejemplo de polimorfismo y herencia solo retornamos un mensaje mostrando los atributos
    public String getDetalle()
    {
        return "El Nombre y apellido de la persona" + this.Nombre + " " + this.Apellido + " su altura: " + this.Altura + " y su peso" + this.Peso;
    }
}
